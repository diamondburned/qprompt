# qprompt

A wrapper around [promptui](https://godoc.org/github.com/manifoldco/promptui)'s messy API. This should only be used for small CLI tools.

This package also introduces two validators, `All` and `Either`. These functions allow stacks of other validators. `Either` is likely what most poeple would use, as it fails as soon as a validator does.

## Howto

- [qprompt API](https://godoc.org/gitlab.com/diamondburned/qprompt)
- [Validator API](https://godoc.org/gitlab.com/diamondburned/qprompt/validator)

## Example

```go
r, err := qprompt.Prompt(
	"What's your favorite food?", "",
	validator.Either(validator.UnicodeFn(unicode.IsLetter), validator.Max(50)),
)

if err != nil {
	// crash and burn
}
```

