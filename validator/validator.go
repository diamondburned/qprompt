package validator

import (
	"fmt"
	"strconv"

	"gitlab.com/diamondburned/qprompt"
)

// Either validates through a list of validators and would mark false as
// soon as a validator fails. This works similarly to AND, in which if one
// validator fails, the entire thing fails.
func Either(vs ...qprompt.Validator) qprompt.Validator {
	return func(s string) error {
		for _, v := range vs {
			if err := v(s); err != nil {
				return err
			}
		}

		return nil
	}
}

// All validates through a list of validators and would mark false
// only if all validators fail. This works similarly to OR, in which if not
// all validators fail, the validator would pass.
func All(vs ...qprompt.Validator) qprompt.Validator {
	return func(s string) error {
		var err error
		for _, v := range vs {
			if err = v(s); err == nil {
				return nil
			}
		}

		return err
	}
}

// Not negates a validator and return the error given when appropriate.
func Not(v qprompt.Validator, err error) qprompt.Validator {
	return func(s string) error {
		if v(s) != nil {
			return nil
		}

		return err
	}
}

// Number validates if the input is a number or not.
func Number() qprompt.Validator {
	return func(s string) error {
		_, err := strconv.Atoi(s)
		return err
	}
}

// UnicodeFn takes in a function that checks all runes in the input
// string. This is meant to be used with package unicode's functions.
func UnicodeFn(f func(rune) bool) qprompt.Validator {
	return func(s string) error {
		for _, r := range []rune(s) {
			if !f(r) {
				return fmt.Errorf("Character %v not in the Unicode range", r)
			}
		}

		return nil
	}
}

// Max sets the maximum rune the input string could be.
func Max(max int) qprompt.Validator {
	return func(s string) error {
		if len([]rune(s)) > max {
			return fmt.Errorf("Input exceeded maximum length of %v", max)
		}

		return nil
	}
}
