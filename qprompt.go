package qprompt

import "github.com/manifoldco/promptui"

var (
	// VimMode sets the default vim mode
	VimMode = false

	// Confirms asks for confirmation
	Confirms = false

	// ShowHelp sets whether or not help should be shown
	ShowHelp = true

	// PromptTmpl sets the default prompt template
	PromptTmpl *promptui.PromptTemplates

	// SelectTmpl sets the default select template
	SelectTmpl *promptui.SelectTemplates

	// ListSize is the default number of items to list out.
	ListSize = 5
)

// Validator is the validate function signature
type Validator = func(string) error

// Prompt spawns a prompt. `def' could be empty for no defaults.
func Prompt(label, def string, validate Validator) (string, error) {
	return (&promptui.Prompt{
		Label:     label,
		Default:   def,
		Validate:  validate,
		Templates: PromptTmpl,
		IsConfirm: Confirms,
		IsVimMode: VimMode,
	}).Run()
}

// SelectStrings uses the default settings to print out a list prompt.
func SelectStrings(label string, items []string) (int, string, error) {
	return (&promptui.Select{
		Label:     label,
		Items:     items,
		Size:      ListSize,
		HideHelp:  !ShowHelp,
		Templates: SelectTmpl,
	}).Run()
}
